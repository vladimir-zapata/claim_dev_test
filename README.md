*HOW TO RUN*

**Project version: .NET 8**

*Database creation script is in project's root directory

**Setting up database**
- Execute CREATE DATABASE DCM_DEV_TEST
- Execute the rest of the db creation script

**Adjust connection string**
- In ClaimProcessorContext class, adjust connection string:
Server=YourServer;Database=DCM_DEV_TEST;Trusted_Connection=True;TrustServerCertificate=True;
	- In case you have a db user take that in consideration.
