﻿using DCM_Dev_Test.ClaimProcessor.Entities.Auditing;
using DCM_Dev_Test.ClaimProcessor.Entities.Pricing;
using Microsoft.EntityFrameworkCore;

namespace DCM_Dev_Test.ClaimProcessor.Context
{
    public class ClaimProcessorContext : DbContext
    {
        #region Audit
        public DbSet<Header> Headers { get; set; }
        public DbSet<LineItem> LineItems { get; set; }
        public DbSet<RuleHit> RuleHits { get; set; }
        public DbSet<ClaimFlag> ClaimFlags { get; set; }
        public DbSet<ClaimPricing> ClaimPricings { get; set; }
        #endregion

        #region Pricing
        public DbSet<DrgWeight> DrgWeights_V36 { get; set; }
        //public DbSet<DrgWeight> DrgWeights_V38 { get; set; }
        public DbSet<TxHospitalRate> TxHospitalRates { get; set; }
        public DbSet<LesserOfTheBilled> LesserOfTheBilled { get; set; }
        public DbSet<CHIRPHospital> CHIRPHospitals_FY22 { get; set; }
        #endregion


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=Dexter;Database=DCM_DEV_TEST;Trusted_Connection=True;TrustServerCertificate=True;");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Header>().HasNoKey();
            builder.Entity<LineItem>().HasNoKey();
            builder.Entity<DrgWeight>().HasNoKey();
            builder.Entity<TxHospitalRate>().HasNoKey();
            builder.Entity<LesserOfTheBilled>().HasNoKey();
            builder.Entity<CHIRPHospital>().HasNoKey();
        }
    }
}
