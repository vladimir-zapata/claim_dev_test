﻿using DCM_Dev_Test.ClaimProcessor.Context;
using DCM_Dev_Test.ClaimProcessor.Entities.Auditing;
using DCM_Dev_Test.ClaimProcessor.Entities.Pricing;

namespace DCM_Dev_Test.ClaimProcessor
{
    public class ClaimPriceProcessor
    {
        private readonly ClaimProcessorContext _context;

        public ClaimPriceProcessor(ClaimProcessorContext context)
        {
            _context = context;
        }

        public void ProcessClaims(List<Header> claims)
        {
            foreach (var claim in claims)
            {
                var hospital = _context.TxHospitalRates.FirstOrDefault(x => x.NPI == claim.NPI);
                var drgWeigth_V36 = _context.DrgWeights_V36.FirstOrDefault(x => x.APRDRGSOI == claim.Score);

                var baseDRG = CalculateBaseDRG(hospital, drgWeigth_V36);
                var outlier = CalculateOutlier(claim.Age < 21, baseDRG, hospital?.HospitalClass);

                double contractReimbursement = CalculateContractReimbursement(baseDRG, outlier, hospital);
                double chirpReimbursement = CalculateChirpReimbursement(contractReimbursement, hospital);
                double totalClaimReimbursement = Math.Round(CalculateTotalClaimReimbursement(chirpReimbursement, contractReimbursement), 2);

                var claimPricing = new ClaimPricing()
                {
                    ClaimID = claim.ClaimID,
                    TotalCharges = claim.TotalCharges,
                    TotalReimbursement = totalClaimReimbursement,
                    TotalToPay = Math.Round(claim.TotalCharges - totalClaimReimbursement, 2)
                };

                SaveClaimPricing(claimPricing);
            }
        }

        private static double? CalculateBaseDRG(TxHospitalRate? hospital, DrgWeight? drgWeight_V36)
        {
            return hospital?.SDA * drgWeight_V36?.RelativeWeight; 
        }

        private static double CalculateOutlier(bool isUnder21, double? baseDRG, string? hospitalClass)
        {
            if (isUnder21 && baseDRG.HasValue)
            {
                var outlier = baseDRG.Value * 0.10;

                if (hospitalClass != null && (hospitalClass.ToLower() == "urban" || hospitalClass.ToLower() == "rural"))
                {
                    outlier *= 0.90;
                }

                return outlier;
            }

            return 0;
        }

        private double CalculateContractReimbursement(double? baseDRG, double outlier, TxHospitalRate? hospital)
        {
            if (hospital == null || !hospital.PPRPPC.HasValue || !hospital.Contract.HasValue)
            {
                return 0;
            }

            return baseDRG + outlier * hospital.PPRPPC.Value * hospital.Contract.Value ?? 0;
        }

        private double CalculateChirpReimbursement(double? contractReimbursement, TxHospitalRate? hospital)
        {
            if (!contractReimbursement.HasValue || hospital == null || !hospital.IPRate.HasValue)
            {
                return 0;
            }

            return contractReimbursement * hospital.IPRate ?? 0;
        }

        public double CalculateTotalClaimReimbursement(double chirpReimbursement, double contractReimbursement)
        {
            return chirpReimbursement + contractReimbursement;
        }

        private void SaveClaimPricing(ClaimPricing claimPricing)
        {
            _context.ClaimPricings.Add(claimPricing);
            _context.SaveChanges();
        }
    }
}
