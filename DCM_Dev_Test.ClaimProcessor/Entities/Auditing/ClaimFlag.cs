﻿using System.ComponentModel.DataAnnotations;

namespace DCM_Dev_Test.ClaimProcessor.Entities.Auditing
{
    public class ClaimFlag
    {
        [Key]
        public int ClaimFlagID { get; set; }
        public int ClaimID { get; set; }
        public string? Flag { get; set; }
    }
}
