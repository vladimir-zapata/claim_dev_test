﻿namespace DCM_Dev_Test.ClaimProcessor.Entities.Auditing
{
    public class LineItem
    {
        public int ClaimID { get; set; }
        public int? Position { get; set; }
        public string? DGXCode { get; set; }
        public int DgxAffect { get; set; }
        public string? DgxCcMcc { get; set; }
    }
}
