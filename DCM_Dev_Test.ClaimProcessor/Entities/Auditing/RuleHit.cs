﻿using System.ComponentModel.DataAnnotations;

namespace DCM_Dev_Test.ClaimProcessor.Entities.Auditing
{
    public class RuleHit
    {
        [Key]
        public int RuleHitId { get; set; }

        public int ClaimID { get; set; }
        public int RuleID { get; set; }
    }
}
