﻿using System.ComponentModel.DataAnnotations;

namespace DCM_Dev_Test.ClaimProcessor.Entities.Auditing
{
    public class ClaimPricing
    {
        [Key]
        public int ClaimPricingId { get; set; }
        public int ClaimID { get; set; }
        public double TotalCharges { get; set; }
        public double TotalReimbursement { get; set; }
        public double TotalToPay { get; set; }
    }
}
