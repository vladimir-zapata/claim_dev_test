﻿namespace DCM_Dev_Test.ClaimProcessor.Entities.Auditing
{
    public class Header
    {
        public int ClaimID { get; set; }
        public string? Score { get; set; }
        public string? NPI { get; set; }
        public DateTime DOB { get; set; }
        public int Age { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LOS { get; set; }
        public double TotalCharges { get; set; }
    }
}
