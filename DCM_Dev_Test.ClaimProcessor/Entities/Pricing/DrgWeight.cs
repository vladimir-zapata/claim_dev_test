﻿namespace DCM_Dev_Test.ClaimProcessor.Entities.Pricing
{
    public class DrgWeight
    {
        public int APRDRG { get; set; }
        public int SOI { get; set; }
        public string? APRDRGSOI { get; set; }
        public string? DRGDescription { get; set; }
        public double RelativeWeight { get; set; }
        public double MeanLengthOfStay { get; set; }
        public int DayOutlierThreshold { get; set; }
    }
}
