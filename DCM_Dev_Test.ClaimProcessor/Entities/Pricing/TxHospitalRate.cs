﻿namespace DCM_Dev_Test.ClaimProcessor.Entities.Pricing
{
    public class TxHospitalRate
    {
        public string? NPI { get; set; }
        public string? Month { get; set; }
        public string? NPIMonth { get; set; }
        public double? IPRate { get; set; }
        public string? ProviderName { get; set; }
        public string? HospitalPhysicalCity { get; set; }
        public string? HospitalPhysicalStreetAddress { get; set; }
        public string? HospitalClass { get; set; }
        public double? SDA { get; set; }
        public double? DeliverySDA { get; set; }
        public double? PPRPPC { get; set; }
        public double? Contract { get; set; }
        public DateTime? HHSCPublishDate { get; set; }
        public double? CHIRPRate { get; set; }
    }
}
