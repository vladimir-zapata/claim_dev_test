﻿namespace DCM_Dev_Test.ClaimProcessor.Entities.Pricing
{
    public class LesserOfTheBilled
    {
        public string? Hospital { get; set; }
        public string? LesserOfBilledChargesIP { get; set; }
        public string? NPI { get; set; }
    }
}
