﻿namespace DCM_Dev_Test.ClaimProcessor.Entities.Pricing
{
    public class CHIRPHospital
    {
        public string Vendor { get; set; }
        public double? TIN { get; set; }
        public string SDA { get; set; }
        public string CHIRPClass { get; set; }
        public float? IP { get; set; }
        public float? OP { get; set; }
        public string NPI { get; set; }
    }
}
