﻿using DCM_Dev_Test.ClaimProcessor;
using DCM_Dev_Test.ClaimProcessor.Context;
using Microsoft.EntityFrameworkCore;

using var context = new ClaimProcessorContext();

context.Database.ExecuteSqlRaw("TRUNCATE TABLE ClaimFlags");
context.Database.ExecuteSqlRaw("TRUNCATE TABLE RuleHits");
context.Database.ExecuteSqlRaw("TRUNCATE TABLE ClaimPricings");

var auditProcessor = new ClaimAuditProcessor(context);
var priceProcessor = new ClaimPriceProcessor(context);

var claims = context.Headers.ToList();

auditProcessor.ProcessClaims(claims);
priceProcessor.ProcessClaims(claims);

foreach (var claim in claims)
{
    var ruleHits = context.RuleHits.Where(r => r.ClaimID == claim.ClaimID).ToList();
    var claimFlags = context.ClaimFlags.Where(cf => cf.ClaimID == claim.ClaimID).ToList();
    var claimPricing = context.ClaimPricings.FirstOrDefault(cp => cp.ClaimID == claim.ClaimID);

    Console.WriteLine("--------------------------------------------");

    Console.WriteLine($"Claim ID: {claim.ClaimID}");
    Console.WriteLine();
    Console.WriteLine("Rules Hit:");

    foreach (var ruleHit in ruleHits)
    {
        Console.WriteLine($" - Rule ID: {ruleHit.RuleID}");
    }

    Console.WriteLine();
    Console.WriteLine("Flags:");

    foreach (var claimFlag in claimFlags)
    {
        Console.WriteLine($" - {claimFlag.Flag}");
    }

    Console.WriteLine();
    Console.WriteLine($"Pricing Information:");

    if (claimPricing != null)
    {
        Console.WriteLine($"Total claim charges: {claimPricing.TotalCharges}");
        Console.WriteLine($"Total claim reimbursement: {claimPricing.TotalReimbursement}");
        Console.WriteLine($"Total to pay: {claimPricing.TotalToPay}");
    }

    Console.WriteLine("--------------------------------------------");
}

