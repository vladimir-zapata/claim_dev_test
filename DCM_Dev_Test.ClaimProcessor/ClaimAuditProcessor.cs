﻿using DCM_Dev_Test.ClaimProcessor.Context;
using DCM_Dev_Test.ClaimProcessor.Entities.Auditing;


namespace DCM_Dev_Test.ClaimProcessor
{
    public class ClaimAuditProcessor
    {
        private readonly ClaimProcessorContext _context;

        public ClaimAuditProcessor(ClaimProcessorContext context)
        {
            _context = context;
        }

        public void ProcessClaims(List<Header> claims)
        {
            foreach (var claim in claims)
            {
                var lineItems = _context.LineItems
                    .Where(li => li.ClaimID == claim.ClaimID && li.Position != null) // How can I treat null values?
                    .OrderBy(li => li.Position) 
                    .ToList();

                ProcessSingleICDRule(claim, lineItems);
                ProcessSingleAlwaysAuditScoreRule(claim);
            }
        }

        private void ProcessSingleICDRule(Header claim, List<LineItem> lineItems)
        {
            var firstItem = lineItems.Where(li => li.Position != 1 && li.DgxAffect == 3)
                                     .OrderBy(li => li.Position)
                                     .FirstOrDefault();

            //Ask: how should we treat null values in position field?

            if (firstItem != null)
            {
                if ((firstItem.DgxCcMcc == "CC" && lineItems.Count(li => li.Position != 1 && li.DgxCcMcc == "CC") == 1) ||
                    (firstItem.DgxCcMcc == "MCC" && lineItems.Count(li => li.Position != 1 && li.DgxCcMcc == "MCC") == 1))
                {
                    if (claim.Score == "720-4" && firstItem.DGXCode == "I248")
                    {
                        SaveRuleHit(claim.ClaimID, -1);
                    }
                    else
                    {
                        SaveRuleHit(claim.ClaimID, 1);
                        FlagClaim(claim);
                    }
                }
            }
        }


        private void ProcessSingleAlwaysAuditScoreRule(Header claim)
        {
            if (claim.Score == "720-4")
            {
                SaveRuleHit(claim.ClaimID, 2);
                SaveClaimFlag(claim.ClaimID, "Human Review");
            }
        }

        private void SaveRuleHit(int claimID, int ruleID)
        {
            _context.RuleHits.Add(new RuleHit { ClaimID = claimID, RuleID = ruleID });
            _context.SaveChanges();
        }

        private void FlagClaim(Header claim)
        {
            string flag;

            if (claim.LOS <= 5)
            {
                flag = "Audit";
            }
            else if (claim.LOS >= 10)
            {
                flag = "Close";
            }
            else
            {
                flag = "Human Review";
            }

            SaveClaimFlag(claim.ClaimID, flag);
        }

        private void SaveClaimFlag(int claimID, string flag)
        {
            _context.ClaimFlags.Add(new ClaimFlag { ClaimID = claimID, Flag = flag });
            _context.SaveChanges();
        }
    }
}
